# GerritCI

Continuous integration for PHP projects using composer.

## Use composer for development and production

The goals for this project is to have one single composer.json file to be used
 in both development and production.
- For development we need a few packages installed locally
- For production we like composer to get all data from remote GIT repositories

## Usage

- Setup your composer.json file for development
  - Use `@dev` or `dev-master` as tag name
  - Add a `repository` with 
    - `type: 'path'`
    - `url: '../../packages/my-package'`, reference you locally cloned package

>Example:
> ```json
> {
>      "require": {
>          "my-vendor/my-package": "@dev"
>      },
>      "repositories": [
>          {
>              "name": "my-vendor/my-package",
>              "type": "path",
>              "url": "../../packages/my-package",
>              "options": {
>                 "symlink": false
>             }
>          }
>      ]
>  }
> ```

- Add production configuration to the `"extra": { }` part
  - GerritCI will overwrite the `tag`, `type` and `url` values 
  in the `require` section, linked by the package name  
```json
{
    "extra": {
        "gerritci": {
            "my-vendor/my-package": {
                "tag": "1.0.*",
                "repository": {
                    "type": "vcs",
                    "url": "git@gitlab.com:sandergerritsen/my-package.git"
                }
            }
        }
    }
}
```
- After successfully running `gerritci` command line, run `composer update` 
 to update the composer.lock file
  - Or maybe better: run `composer update phpunit/phpunit`. Select any 
  package from your `require-dev` section. That will only update that single
  package. And as this is a dev requirement only, it will not unintentionally 
  update any packages used in a production environment. 